# Parco Food

Esta aplicación móvil fue hecha con React Native, usando la cámara del dispositivo para escanear códigos de barra 128 para consumir datos de la API de [Open Food Facts](https://world.openfoodfacts.org/data).

## Instalación
Para ejecutar el proyecto se deben de instalar las dependencias de Node correspondientes:

```bash
npm i
```

Instalar pods:

```bash
npx pod install
```

## Ejecución

Conectar un dispositivo android a la PC y ejecutar el siguiente comando dentro del directorio del proyecto:

```bash
npx react-native run-android
```

## Uso

Basta con comenzar a escanear códigos de barra con IDs válidos para la API de [Open Food Facts](https://world.openfoodfacts.org/data). Aquí un par de ejemplos.

![Nutella](./Barcodes/Nutella.jpg)  

</br>
</br>
</br>
</br>

![Nutella](./Barcodes/Bugles.jpg)  

</br>
</br>
</br>
</br>

![Nutella](./Barcodes/Ruffles.jpg)  