import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://world.openfoodfacts.org/api/v2/'
});

export default instance;
