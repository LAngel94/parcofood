import React, { useEffect, useState } from 'react';
import {
    ActivityIndicator,
    Button,
    Image,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View
} from 'react-native';
import { connect } from 'react-redux';
import AwesomeAlert from 'react-native-awesome-alerts';
import { SafeAreaView } from 'react-native-safe-area-context';

import openFoodFactsAPI from '../api/openFoodFacts';
import * as mapDispatchToProps from './../redux/actions';

const Product = ({
    navigation: { getParam, popToTop },
    addProduct,
    modifyProductQuantity,
    shoppingCart
}) => {
    const [loading, setLoading] = useState(true);
    const [product, setProduct] = useState(null);
    const [error, setError] = useState('');
    const [quantity, setQuantity] = useState('1');
    const [isDuplicatedAlertVisible, setisDuplicatedAlertVisible] =
        useState(false);

    const productId = getParam('productId');

    useEffect(() => {
        const fetchProduct = async () => {
            try {
                const response = await openFoodFactsAPI.get(
                    `product/${productId}`
                );

                if (response.data.status === 0 || !response.data.product) {
                    setError(
                        'The product was not found. Please try another barcode.'
                    );
                    setLoading(false);
                    return;
                }

                setProduct(response.data.product);
                setLoading(false);
            } catch (error) {
                // console.log(error);
                setError('Something went wrong. Please try again later.');
                setLoading(false);
            }
        };

        fetchProduct();
    }, []);

    const onAddButtonPress = () => {
        if (shoppingCart.find(product => product.id === productId)) {
            setisDuplicatedAlertVisible(true);
            return;
        }

        addProduct({
            id: product._id,
            quantity,
            name: product.product_name,
            image: product.image_url
        });
        popToTop();
    };

    if (loading || error) {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                {error && <Text>{error}</Text>}
                {loading && <ActivityIndicator size='large' color='#ff706e' />}
            </View>
        );
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ScrollView style={{ padding: 10 }}>
                <View style={{ marginBottom: 20 }}>
                    <Text style={{ fontSize: 25 }}>{product.product_name}</Text>
                </View>

                <Image
                    source={{
                        uri: product.image_url
                    }}
                    style={{ width: '100%', height: 300, marginBottom: 20 }}
                />

                <View
                    style={{
                        marginBottom: 20,
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}
                >
                    <Text style={{}}>Quantity: </Text>
                    <TextInput
                        value={quantity}
                        keyboardType='numeric'
                        style={{
                            borderWidth: 1,
                            height: 35,
                            width: 35,
                            borderRadius: 5,
                            marginLeft: 5
                        }}
                        onChangeText={quantity => {
                            if (
                                /^[1-9][0-9]?$/.test(quantity) ||
                                quantity === ''
                            )
                                setQuantity(quantity);
                        }}
                        maxLength={2}
                        onBlur={() => {
                            console.log(quantity === '');
                            if (quantity === '') setQuantity('1');
                        }}
                        textAlign='center'
                    />
                </View>

                <Button
                    title='Add to Shopping Cart'
                    color='#ff706e'
                    onPress={onAddButtonPress}
                />

                <AwesomeAlert
                    show={isDuplicatedAlertVisible}
                    showProgress={false}
                    title='Duplicated product'
                    message={`This product is already in your shopping cart. Woud you like to increase the quantity of items by ${quantity} instead?`}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={true}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText='No, go back'
                    confirmText='Yes, add them'
                    confirmButtonColor='#ff706e'
                    onCancelPressed={() => {
                        setisDuplicatedAlertVisible(false);
                    }}
                    onConfirmPressed={() => {
                        const duplicatedProduct = shoppingCart.find(
                            product => product.id === productId
                        );
                        let quantityToSet = parseInt(quantity);

                        if (duplicatedProduct.quantity + quantityToSet > 99) {
                            quantityToSet = 99 - duplicatedProduct.quantity;
                        }

                        modifyProductQuantity(productId, quantityToSet);
                        setisDuplicatedAlertVisible(false);
                        popToTop();
                    }}
                />
            </ScrollView>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({});

const mapStateToProps = state => {
    const { shoppingCart } = state;

    return { shoppingCart };
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
