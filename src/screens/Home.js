import React from 'react';
import {
    Button,
    FlatList,
    Image,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { connect } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import FAIcon from 'react-native-vector-icons/FontAwesome';
import FA5Icon from 'react-native-vector-icons/FontAwesome5';
import FEIcon from 'react-native-vector-icons/Feather';
import * as mapDispatchToProps from './../redux/actions';

const Home = ({
    navigation: { navigate },
    shoppingCart,
    modifyProductQuantity,
    removeProduct
}) => {
    const totalItems = shoppingCart.reduce(
        (accumulator, product) => accumulator + product.quantity,
        0
    );
    const renderProduct = ({ item: product }) => (
        <View
            style={styles.productContainer}
            onPress={() => navigate('Product', { productId: product.id })}
        >
            <Image
                source={{
                    uri: product.image
                }}
                style={styles.productImage}
            />
            <View style={styles.productInfoContainer}>
                <Text style={styles.productName}>{product.name}</Text>

                <View style={styles.productQuantityInfoContainer}>
                    <TouchableOpacity
                        style={styles.productQuantityRemoveButton}
                        onPress={() => {
                            if (product.quantity === 1) return;
                            else modifyProductQuantity(product.id, -1);
                        }}
                    >
                        <FEIcon name='minus' size={25} color='#ff706e' />
                    </TouchableOpacity>

                    <View style={styles.productQuantityContainer}>
                        <Text style={styles.productQuantity}>
                            {product.quantity}
                        </Text>
                    </View>

                    <TouchableOpacity
                        style={styles.productQuantityAddButton}
                        onPress={() => {
                            if (product.quantity === 99) return;
                            else modifyProductQuantity(product.id, 1);
                        }}
                    >
                        <FEIcon name='plus' size={25} color='#ff706e' />
                    </TouchableOpacity>
                </View>
            </View>

            <View style={styles.actionsContainer}>
                <TouchableOpacity
                    onPress={() =>
                        navigate('Product', {
                            productId: product.id
                        })
                    }
                >
                    <FA5Icon name='eye' size={30} color='#ff706e' />
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={() => {
                        removeProduct(product.id);
                    }}
                >
                    <FAIcon name='trash' size={30} color='#ff706e' />
                </TouchableOpacity>
            </View>
        </View>
    );

    return (
        <SafeAreaView style={styles.screenContainer}>
            <FlatList
                data={shoppingCart}
                keyExtractor={product => product.id.toString()}
                renderItem={renderProduct}
                ListEmptyComponent={
                    <View style={styles.emptyListContainer}>
                        <Text style={styles.emptyListMessage}>
                            Your shopping cart is currently empty. Go ahead and
                            scan a barcode to add products here.
                        </Text>
                    </View>
                }
                contentContainerStyle={styles.listContentContainer}
            />

            <View style={styles.cartTotalContainer}>
                <FAIcon name='shopping-cart' size={30} color='#ffdeba' />
                <Text style={styles.cartTotalInfo}>
                    <Text style={{ fontWeight: 'bold' }}>Total</Text>:{' '}
                    {totalItems}{' '}
                    {totalItems > 1 || totalItems === 0 ? 'items' : 'item'}
                </Text>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    screenContainer: { flex: 1 },
    productContainer: {
        flexDirection: 'row',
        margin: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray'
    },
    emptyListContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyListMessage: {
        paddingHorizontal: 10,
        textAlign: 'center',
        color: 'lightgray'
    },
    listContentContainer: { flexGrow: 1 },
    productImage: { height: 100, width: 100 },
    productInfoContainer: {
        paddingHorizontal: 10,
        justifyContent: 'space-around',
        flex: 1
    },
    productName: { color: '#ff706e', fontSize: 16, fontWeight: 'bold' },
    productQuantityInfoContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-start'
    },
    productQuantityContainer: {
        backgroundColor: '#ff706e',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-start',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 5
    },
    productQuantity: { color: '#ffdeba' },
    productQuantityRemoveButton: { paddingRight: 10 },
    productQuantityAddButton: { paddingLeft: 10 },
    actionsContainer: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'space-evenly',
        flex: 1
    },
    cartTotalContainer: {
        paddingVertical: 10,
        backgroundColor: '#ff706e',
        paddingLeft: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    cartTotalInfo: {
        marginLeft: 10,
        fontSize: 20,
        color: '#ffdeba'
    }
});

Home.navigationOptions = ({ navigation: { navigate } }) => ({
    title: 'My Shopping Cart',
    headerRight: () => (
        <TouchableOpacity
            onPress={() => navigate('Scanner')}
            style={{
                marginRight: 10
            }}
        >
            <FAIcon name='camera' size={25} color='#ff706e' />
        </TouchableOpacity>
    )
});

const mapStateToProps = state => {
    const { shoppingCart } = state;

    return { shoppingCart };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
