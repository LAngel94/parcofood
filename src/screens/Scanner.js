import React, { useEffect } from 'react';
import { View, StyleSheet, ActivityIndicator, Linking } from 'react-native';
import { Camera, useCameraDevices } from 'react-native-vision-camera';
import { useScanBarcodes, BarcodeFormat } from 'vision-camera-code-scanner';
import { SafeAreaView } from 'react-native-safe-area-context';

const Scanner = ({ navigation: { navigate } }) => {
    const devices = useCameraDevices();
    const device = devices.back;

    const [frameProcessor, barcodes] = useScanBarcodes([
        BarcodeFormat.CODE_128
    ]);

    const checkPermission = async () => {
        const cameraPermissionStatus = await Camera.getCameraPermissionStatus();

        console.log(cameraPermissionStatus);

        if (cameraPermissionStatus === 'not-determined') {
            const requestResult = await Camera.requestCameraPermission();

            if (requestResult === 'denied') {
                Linking.openSettings();
            }
        } else if (cameraPermissionStatus === 'denied') {
            Linking.openSettings();
        }
    };

    useEffect(() => {
        checkPermission();
    }, []);

    useEffect(() => {
        if (barcodes.length === 1) {
            navigate('Product', {
                productId: barcodes[0].content.data
            });
        }
    }, [barcodes]);

    return !device ? (
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <ActivityIndicator size='large' color='#ff706e' />
        </View>
    ) : (
        <Camera
            style={StyleSheet.absoluteFill}
            device={device}
            isActive={true}
            frameProcessor={frameProcessor}
            frameProcessorFps={5}
        />
    );
};

/* const styles = StyleSheet.create({
    barcodeTextURL: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    }
}); */

Scanner.navigationOptions = { headerShown: null };

export default Scanner;
