import { ADD_PRODUCT, MODIFY_PRODUCT_QUANTITY, REMOVE_PRODUCT } from '../types';

const initialState = [];

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_PRODUCT: {
            return [
                ...state,
                {
                    ...action.payload,
                    quantity: parseInt(action.payload.quantity)
                }
            ];
        }
        case MODIFY_PRODUCT_QUANTITY: {
            const { quantity, productId } = action.payload;

            const productToModify = state.find(
                product => product.id === productId
            );
            productToModify.quantity = productToModify.quantity + quantity;

            return state.map(product => {
                if (product.id === productId) {
                    return productToModify;
                }

                return product;
            });
        }
        case REMOVE_PRODUCT: {
            return state.filter(product => {
                return product.id !== action.payload;
            });
        }
        default: {
            return state;
        }
    }
}
