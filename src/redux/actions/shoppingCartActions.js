import { ADD_PRODUCT, MODIFY_PRODUCT_QUANTITY, REMOVE_PRODUCT } from '../types';

export const addProduct = product => {
    return { type: ADD_PRODUCT, payload: product };
};

export const modifyProductQuantity = (productId, quantity) => {
    return { type: MODIFY_PRODUCT_QUANTITY, payload: { productId, quantity } };
};

export const removeProduct = productId => {
    return { type: REMOVE_PRODUCT, payload: productId };
};
