import React from 'react';
import { StatusBar } from 'react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import reducers from './redux/reducers';
import Home from './screens/Home';
import Scanner from './screens/Scanner';
import Product from './screens/Product';

const store = createStore(reducers);

const MainNavigator = createStackNavigator({
    Home,
    Product
});

const CameraNavigator = createSwitchNavigator({
    MainNavigator,
    Scanner
});

const AppContainer = createAppContainer(CameraNavigator);

const App = () => {
    return (
        <Provider store={store}>
            <StatusBar barStyle={'light-content'} />
            <AppContainer />
        </Provider>
    );
};

export default App;
